<?php

require_once('vendor/autoload.php');

const DB_SERVER = 'localhost';
const DB_USERNAME = 'root';
const DB_PASSWORD = '';
const DB_NAME = 'db';

const REDIS_SCHEME = 'tcp';
const REDIS_HOST = '127.0.0.1';
const REDIS_PORT = 6379;

class UserModel {

	public $id;
	public $username;
	public $groupId;
	public $groupName;

	/**
	 * @return array
	 */
	public static function getAll() {
		$result = [];

		if (RedisManager::exists('usersQueryResult')) {
		    $result = RedisManager::get('usersQueryResult');
		} else {
			$result = DbManager::get(
				"SELECT users.*, group_name FROM users, groups WHERE groups.id = users.group_id"
			);

		    RedisManager::set('usersQueryResult', $result);
		}

		$all = [];

		foreach ($result as $row) {
			$user = new self();

			$user->id = $row['id'];
			$user->username = $row['username'];
			$user->groupId = $row['group_id'];
			$user->groupName = $row['group_name'];

			array_push($all, $user);
		}

		return $all;
	}
}

class View {
	/**
	 * @param mixed $data
	 */
	public function show($data) {
        header("Content-Type: application/json");
		print json_encode($data);
	}
}

class Controller {
	public function launch() {
		$users = UserModel::getAll();
		$view = new View();
		$view->show($users);
	}
}

class RedisManager {
	/**
	 * @param string $key
	 * @return bool
	 */
	public static function exists($key) {
		self::initialize();
		return self::$redis->exists($key);
	}

	/**
	 * @param string $key
	 * @return mixed
	 */
	public static function get($key) {
		self::initialize();
		return json_decode(self::$redis->get($key), true);
	}

	/**
	 * @param string $key
	 * @param mixed $value
	 */
	public static function set($key, $value) {
		self::initialize();
		self::$redis->set($key, json_encode($value));
	}

	private static $redis = null;

	private static function initialize() {
		if (self::$redis !== null) {
			return;
		}

		try {
			self::$redis = new Predis\Client([
				'scheme' => REDIS_SCHEME,
				'host'   => REDIS_HOST,
				'port'   => REDIS_PORT
			]);
		}
		catch (Exception $e) {
			die($e->getMessage());
		}
	}
}

class DbManager {
	/**
	 * @param string $key
	 * @return array
	 */
	public static function get($query) {
		self::initialize();

		$result = self::$mysqli->query($query);
		$resultArray = [];

		while ($row = $result->fetch_assoc()) {
			array_push($resultArray, $row);
		}

		return $resultArray;
	}

	private static $mysqli = null;

	private static function initialize() {
		if (self::$mysqli !== null) {
			return;
		}

		try {
			self::$mysqli = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
		}
		catch (Exception $e) {
			die($e->getMessage());
		}
	}
}

$controller = new Controller();
$controller->launch();
