CREATE TABLE groups (
  id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  group_name TINYTEXT NOT NULL
);


INSERT INTO groups VALUES
(1, 'Administrators'),
(2, 'Users');


CREATE TABLE users (
  id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  username TINYTEXT NOT NULL,
  group_id INT UNSIGNED NOT NULL
);


INSERT INTO users VALUES
(1, 'Ivanov', 1),
(2, 'Petrov', 2),
(3, 'Sidorov', 2);
